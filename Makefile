CC=gcc
CFLAGS=`pkg-config --cflags`
LDFLAGS=`pkg-config --libs`
FLAGS_EXTRA=``

all: clean main

main: main.c
	$(CC) -o main main.c $(CFLAGS) $(LDFLAGS) $(FLAGS_EXTRA)

run: main
	./main

clean:
	rm -f main
	rm -rf *.o

#include <stdlib.h>
#include "mdp.h"


mdp_t *mdp_create(
        int states, 
        int actions, 
        double (*reward)(state_t state), 
        state_t (*transition)(state_t state, action_t action), 
        double discount) {
    mdp_t *mdp = malloc(sizeof(mdp_t));
    mdp->states = malloc(sizeof(state_t) * states);
    mdp->n_states = states;
    mdp->actions = malloc(sizeof(action_t) * actions);
    mdp->reward = reward;
    mdp->transition = transition;
    mdp->policy = mdp_policy_create(discount, mdp);
    return mdp;
}

void mdp_destroy(mdp_t *mpd) {
    mdp_policy_destroy(mdp->policy);
    free(mdp->states);
    free(mdp->actions);
    free(mdp);
}

policy_t *mpd_policy_create(double discount, mdp_t *mdp) {
    policy_t *policy = malloc(sizeof(policy_t));
    policy->discount = discount;
    policy->actions = malloc(sizeof(state_t) * mdp->states);
    policy->utilities = malloc(sizeof(double) * mdp->states);
    policy->mdp = mdp;
    return policy;
}

void mdp_policy_destroy(policy_t *policy) {
    free(policy->actions);
    free(policy->utilities);
    free(policy);
}

void mdp_policy_evaluate(policy_t *policy) {
    policy_t new_policy = mdp_policy_create(policy->discount, policy->mdp); 
    mdp_t *mdp = policy->mdp;

    for (int i = 0; i < mdp->n_states; i++) {
        new_policy->utilities[i] = *(mdp->reward)(i);

        
        for (int j = 0; j < mdp->n_states; j++) {
            new_policy->utilities[i] += policy->discount * *(mdp->transition)(i, policy->actions[i]) * policy->utilities[i];
        }
    } 
}



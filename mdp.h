#ifndef MDP_H
#define MDP_H

typedef struct {
    state_t *states;
    int n_states;
    action_t *actions;
    double (*reward)(state_t state);
    state_t (*transition)(state_t state, action_t action);
    policy_t *policy;
} mdp_t;

typedef struct {
    double discount;
    action_t *actions;
    double *utilities;
    mdp_t *mdp;
} policy_t;

typedef int action_t;
typedef int state_t;

mdp_t *mdp_create(state_t *states, action_t *actions, double (*reward)(state_t state), state_t (*transition)(state_t state, action_t action), double discount);
void mdp_destroy(mdp_t *mpd);

mdp_policy *mpd_policy_create(double discount, mdp_t *mdp);
void mdp_policy_destroy(policy_t *policy);


#endif
